package com.snails.permission.model;

import java.util.List;

/**
 * 权限被拒绝
 *
 * @author lawrence
 * @date 2019-05-05 16:11
 */
public class DenyBean {

    private int requestCode;
    private List<String> denyList;

    public DenyBean(int requestCode, List<String> denyList) {
        this.requestCode = requestCode;
        this.denyList = denyList;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public List<String> getDenyList() {
        return denyList;
    }

    public void setDenyList(List<String> denyList) {
        this.denyList = denyList;
    }

}

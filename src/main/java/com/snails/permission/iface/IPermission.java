package com.snails.permission.iface;

import java.util.List;

/**
 * @author lawrence
 * @date 2019-05-05 16:35
 */
public interface IPermission {

    //同意权限
    void PermissionGranted();

    //拒绝权限并且选中不再提示
    void PermissionDenied(int requestCode, List<String> denyList);

    //取消权限
    void PermissionCanceled(int requestCode);

}

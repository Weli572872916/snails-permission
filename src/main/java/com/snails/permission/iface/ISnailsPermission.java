package com.snails.permission.iface;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * 实现该接口的对象，可以在内部进行权限申请
 *
 * @author lawrence
 * @date 2019-05-08 15:54
 */
public interface ISnailsPermission {
    
    @NonNull Context getCtx();

}

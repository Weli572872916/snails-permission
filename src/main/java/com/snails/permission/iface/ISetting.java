package com.snails.permission.iface;

import android.content.Intent;

/**
 * @author lawrence
 * @date 2019-05-05 16:36
 */
public interface ISetting {
    Intent getSetting();
}

package com.snails.permission.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限取消
 * -- 如果用户没有给权限，但也没有选中不再提示，这种情况称为权限被取消
 *
 * @author lawrence
 * @date 2019-05-05 16:04
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermissionCanceled {

    int requestCode() default 0;

}

package com.snails.permission.support;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.snails.permission.iface.ISetting;

/**
 * @author lawrence
 * @date 2019-05-08
 */
public class HuaWeiSetting implements ISetting {
    private Context mCtx;

    public HuaWeiSetting(Context ctx) {
        this.mCtx = ctx;
    }

    @Override
    public Intent getSetting() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("packageName", mCtx.getPackageName());
        intent.setComponent(new ComponentName("com.huawei.systemmanager",
                "com.huawei.permissionmanager.ui.MainActivity"));
        return intent;
    }
}